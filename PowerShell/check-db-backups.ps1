Import-Module AWS.Tools.S3

function CheckBackups([string]$bucket, [string[]]$dbNames) 
{
	Set-StrictMode -Version Latest

	$endDate = (Get-Date).AddDays(-1)
	$startDate = $endDate.AddMonths(-1)

	$startPrefix = $endDate.ToString("MMMM") + "_" + $endDate.ToString("yyyy")
	$endPrefix = $startDate.ToString("MMMM") + "_" + $startDate.ToString("yyyy")

	$objectList = @(get-s3object $bucket -ProfileName Admin -Prefix $startPrefix)
	$objectList += @(get-s3object $bucket -ProfileName Admin -Prefix $endPrefix)

	foreach ($dbName in $dbNames)
	{
		Write-Host "Checking backups of $dbName in $bucket."
		
		$names = $objectList `
			| Select-Object -ExpandProperty Key `
			| Select-String "$($dbName)_backup_(\d{4}_\d{2}_\d{2})" -All `
			| select-Object @{l="Value"; e={$_.Matches[0].Groups[1].Value }}  `
			| Select-Object -ExpandProperty Value 

		$datesNotFound = @()
		for ($d = $startDate; $d -le $endDate; $d = $d.AddDays(1)) { 
			$dateStr = $d.ToString("yyyy_MM_dd");
			if ($names -notcontains $dateStr) {
				$datesNotFound += $dateStr.Replace("_", "-")
			}
		}

		$endDateStr = $endDate.ToString("yyyy-MM-dd")
		if ($datesNotFound -contains $endDate.ToString("yyyy-MM-dd")) {
			Write-Host -ForegroundColor red "T-1 backup for $endDateStr was not found" 
		} else {
			Write-Host -ForegroundColor green "T-1 backup for $endDateStr was found" 
		}

		$datesNotFound `
			| Where-Object { $_ -ne $endDateStr} `
			| ForEach-Object { Write-Host -ForegroundColor red "Backup for $_ not found." } `
	}
}


CheckBackups "irate-db-daily-backups-prod" "IRate"
CheckBackups "irate-db-daily-backups-prod" "IRate_01"
CheckBackups "irate-db-daily-backups-prod" "IRate_02"

CheckBackups "irate-db-daily-backups-dev" "IRate"
CheckBackups "irate-db-daily-backups-dev" "InvescoAnalytics"
