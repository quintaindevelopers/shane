# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 16:47:42 2020

@author: ShaneMcPherson
"""

import numpy as np
import pandas as pd
import sqlite3
import sys

pd.set_option('display.width', 500)
pd.set_option("display.max_rows", 10000) 
pd.set_option("display.max_columns", 500)

nan = np.nan

interactions =  pd.read_excel("D:/home/shane/Sync/Impax AA data/shane/Impax_Interactions_Combined column structure_post_SM.xlsx", sheet_name = 'Worksheet')



#%%

columns = ["Date", "Provider", "InteractionCategory", "InvestorAttendees", "Sellside person", "Description"]

#interactions.rename({"Sellside person" : "SellsideAnalyst"}, axis=1, inplace=True)

interactions["RowId"] = interactions.index
interactions.InvestorAttendees.fillna("", inplace=True)
interactions["Sellside person"].fillna("", inplace=True)

interactions["GroupId"] = interactions.groupby(columns).ngroup()
interactions["FirstRowIdInGroup"] = interactions.groupby(columns)['RowId'].transform('min')
interactions["DuplicateCount"] = interactions.groupby(columns)['RowId'].transform('count')
interactions["IsDuplicate"] = interactions.RowId != interactions.FirstRowIdInGroup
interactions.sort_values("RowId", inplace=True)

interactions.to_excel("d:/temp/impax_duplicate_interactions_marked.xlsx")


"""
stats = repeats.groupby(["Date", "Provider", "InteractionCategory", "InvestorAttendees", "Sellside person"]).agg({"GroupId" : "min", "Date" : "count"})
stats.columns = ["GroupId", "Count"]
stats = stats.reset_index().sort_values("GroupId")

writer = pd.ExcelWriter(f"d:/temp/impax_repeats.xlsx", engine='xlsxwriter')    
stats.to_excel(writer, sheet_name="Counts")
repeats.to_excel(writer, sheet_name="RepeatInteractions")
writer.save()
"""