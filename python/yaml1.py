# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 15:37:33 2020

@author: shane
"""

import yaml
from pypika import Table, Query
import os
import pyodbc

import pykwalify

#c = pykwalify.core.Core(source_file="simple.yaml", schema_files=["schema.yaml"])

def read_yaml():
    with open("test.yaml", 'r') as stream:
        try:            
            c = pykwalify.core.Core(source_file="simple.yaml", schema_files=["schema.yaml"])            
            return  c.validate(raise_exception=True)
        except yaml.YAMLError as exc:
            print(exc)
    

    
def main():
    data = read_yaml()
         
    cxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=127.0.0.1,55500;DATABASE=IRate;UID=sa;PWD=Analec123', autocommit =True) 
    cursor = cxn.cursor()
    
    cursor.execute("select max(assertation_question_id) as max_id from  assertation_question;")
    max_assertation_question_id = cursor.fetchone().max_id
    
    cursor.execute("select max(assertation_option_id) as max_id from  assertation_option;")
    max_assertation_option_id = cursor.fetchone().max_id             
        
    assertation_question = Table('assertation_question') 
    assertation_question_config = Table('assertation_question_config')        
    assertation_option = Table('assertation_option')         
    assertation_question_team_mapping = Table('assertation_question_team_mapping')
    
    client_institution_id = 102230
    
    i = max_assertation_question_id + 1
    j = max_assertation_option_id + 1
    
    questions = data['assertation-questions']
    
    for aq in questions:
        allow_multi = aq.get('allow-multi', 0)
        is_required = aq.get('is-required', 0)
        info_prompt = aq.get('additional-info-prompt', 0)
        additional_info_mandatory = aq.get('additional-info-mandatory', 0)
        q = (Query.into(assertation_question)
            .columns('assertation_question_id', 'name', 'allow_multi_select')
            .insert(i, aq['name'], allow_multi))
        print(q.get_sql(quote_char=None))
        
        q = (Query
             .into(assertation_question_config)
             .columns('institution_id', 'assertation_question_id', 'is_required',
                 'is_additional_info_always_required', 'additional_info_prompt')
             .insert(client_institution_id, i, is_required, additional_info_mandatory, info_prompt))
        print(q.get_sql(quote_char=None))        
        
        
        for opt in aq.get("options", []):
            require_additional_info = opt.get('require-additional_info', 0)
            short_name = opt.get('short-name', None)
            q = (Query.into(assertation_option)
                .columns('assertation_option_id', 'assertation_question_id', 'name', 
                    'require_addition_info', 'short_name')
                .insert(j, i, opt['name'], require_additional_info, short_name))
            j += 1
            print(q.get_sql(quote_char=None))
        i += 1
            
    for m in data['assertation-question-team-mappings']:
        team_ref, team_ref_type = get_reference_value(m["team"])
                    
        display_order = 0
        for item in m["questions"]:            
            q, aq_ref, aq_ref_type = find_item_by_id_or_tag(questions, item, "Assertation question")
            if not q: continue
                        
            assertation_question_id = (
                aq_ref if aq_ref_type == "id" 
                else max_assertation_question_id + questions.index(q) + 1)
                                    
            sql = (Query.into(assertation_question_team_mapping)
                   .columns("assertation_question_id", "team_id", "display_order")
                   .insert(assertation_question_id, team_ref, display_order)).get_sql(quote_char=None)
            print(sql)
            display_order += 1
            
def get_reference_value(referer):
    tag =  referer.get("tag")
    return (tag, "tag") if tag else (referer["id"], "id")
                       
def find_item_by_id_or_tag(items, referer, item_type):    
    ref, ref_type = get_reference_value(referer)
    item = next((x for x in items if x[ref_type] == ref), None)
    if not item: print(f"Error: {item_type} with {ref_type}: {ref} not found.")
    return item, ref, ref_type
                
                 
#%%
            
if __name__ == "__main__":
    main()
  