# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 14:55:53 2020

@author: ShaneMcPherson
"""


import numpy as np
import pandas as pd
import sqlite3
import sys

pd.set_option('display.width', 500)
pd.set_option("display.max_rows", 10000) 
pd.set_option("display.max_columns", 500)

nan = np.nan

interactions =  pd.read_excel("D:/home/ShaneMcpherson/Sync//Impax AA data/shane/Impax_Interactions_Combined column structure_post_SM.xlsx", sheet_name = 'Worksheet')

#%%

columns = ["Date", "Provider", "InteractionCategory", "InvestorAttendees", "SellsideAnalyst", "Description"]

interactions.rename({"Sellside person" : "SellsideAnalyst"}, axis=1, inplace=True)
repeats = interactions.groupby(columns).filter(lambda x: len(x) > 1)
repeats["GroupId"] = repeats.groupby(columns).ngroup()
repeats = repeats.sort_values("GroupId")

stats = repeats.groupby(["Date", "Provider", "InteractionCategory", "InvestorAttendees", "SellsideAnalyst"]).agg({"GroupId" : "min", "Date" : "count"})
stats.columns = ["GroupId", "Count"]
stats = stats.reset_index().sort_values("GroupId")

writer = pd.ExcelWriter(f"d:/temp/impax_duplicates.xlsx", engine='xlsxwriter')    
stats.to_excel(writer, sheet_name="Counts")
repeats.to_excel(writer, sheet_name="DuplicateInteractions")
writer.save()