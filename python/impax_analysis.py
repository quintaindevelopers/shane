# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 18:26:51 2020

@author: ShaneMcPherson
"""

#%%

import numpy as np
import pandas as pd
import sqlite3
import sys
from pandas.testing import assert_frame_equal

 #pylint: disable=abstract-class-instantiated

pd.set_option('display.width', 500)
pd.set_option("display.max_rows", 10000) 
pd.set_option("display.max_columns", 500)

def write_df_to_excel(writer, df, sheetname, include_index=True):
        pct_format = writer.book.add_format({'num_format': '0%'})
        
        if include_index: df = df.reset_index()
        
        df.to_excel(writer, sheet_name=sheetname, index=False)
        worksheet = writer.sheets[sheetname]
        for idx, col in enumerate(df):
            series = df[col]            
            max_len = max((
                np.nan_to_num(series.astype(str).map(len).max(), 0),  # len of largest item
                len(str(series.name))  # len of column name/header
            )) + 3  # adding a little extra space
            
            if 'Pct' in series.name:
                worksheet.set_column(idx, idx, max_len, pct_format)
            else:
                worksheet.set_column(idx, idx, max_len)
    
def write_df_to_excel_2(writer, df, sheetname, max_col_width=None):    
    df = df.reset_index()
    
    df.to_excel(writer, sheet_name=sheetname)
    worksheet = writer.sheets[sheetname]
    
    pct_format = writer.book.add_format({'num_format': '0%'})
    
    for idx, col in enumerate(df):
        series = df[col]       
             
        max_len = max((
            np.nan_to_num(series.astype(str).map(len).max(), 0),  # len of largest item
            len(series.name) if isinstance(series.name, str) else max([len(str(s)) for s in series.name])             
        )) + 3  # adding a little extra space      
                
        if max_col_width != None: 
            max_len = min(max_col_width, max_len)
        
        if 'Pct' in series.name: #series.name[len(series.name) - 1]:
            worksheet.set_column(idx + 1, idx + 1, max_len, pct_format)  
        else:
            worksheet.set_column(idx + 1, idx + 1, max_len)

def read_data():
    nan = np.nan
    
    sync_dir = "D:/home/shane/Sync"

    interaction_data =  pd.read_excel(sync_dir + "/Impax AA data/shane/Impax_Interactions_Combined column structure_post_SM.xlsx", sheet_name = 'Worksheet')
    
    interaction_data = interaction_data.query("InteractionCategory in ['Research Meeting', 'Model Request', 'Custom Request']").copy()
    interaction_data["ProviderContact"] = interaction_data['Sellside person'].fillna("").apply(lambda x: next(iter(x.split(",")), "").strip())
    interaction_data["InvestorContact"] = interaction_data['InvestorAttendees'].fillna("").apply(lambda x: next(iter(x.split(",")), "").strip()).drop(columns=['Firm Participants', 'Provider Contacts'])
    interaction_data = interaction_data[['Date', 'InteractionCategory', 'Provider', 'ProviderContact', 'InvestorContact', 'Description', "InvestorAttendees", "ImpaxSpecificTag"]]
    interaction_data.insert(0, "Quarter", interaction_data.Date.dt.quarter)
    interaction_data.insert(0, "Year", interaction_data.Date.dt.year)
    interaction_data.insert(0, "InteractionRowId", interaction_data.index + 1)

    readership_data= pd.read_excel(sync_dir + "/Impax AA data/shane/Impax_Research readership data file from client_SM.xlsx", sheet_name = 'Readership')
    
    readership_data = readership_data[['Date', 'InteractionSubType',  'Provider', 'Provider Contacts', 'Firm Participants', 'Description']].query("InteractionSubType in ['Research Read', @nan]").copy()
    readership_data["ProviderContact"] = readership_data['Provider Contacts'].fillna("").apply(lambda x: next(iter(x.split(",")), "").strip())
    readership_data["InvestorContact"] = readership_data['Firm Participants'].fillna("").apply(lambda x: next(iter(x.split(",")), "").strip())
    readership_data = readership_data.drop(columns=['Firm Participants', 'Provider Contacts'])
    readership_data.InteractionSubType = readership_data.InteractionSubType.fillna("")
    readership_data.insert(0, "Quarter", readership_data.Date.dt.quarter)
    readership_data.insert(0, "Year", readership_data.Date.dt.year)
    readership_data.insert(0, "ReadershipRowId", readership_data.index + 1)
     
    return interaction_data, readership_data

#%%

def compute(interaction_data, readership_data):
    rename_map = {'Provider': {"BofA": "Bank of America",  "BofAML": "Bank of America", "BofA Merrill Lynch": "Bank of America"}}    
    
    interaction_data = interaction_data.replace(rename_map)
    readership_data = readership_data.replace(rename_map)

    readership_data_all = readership_data.copy()
            
    df0 = readership_data_all.groupby(["Year", "Quarter", "Provider"]).size().rename("Count").to_frame()
    df0a = readership_data_all.groupby(["Year", "Quarter", "Provider", "ProviderContact"]).size().rename("Count").to_frame()
    
    interaction_data = interaction_data[interaction_data["ProviderContact"] != ""]
    readership_data = readership_data[readership_data["ProviderContact"] != ""]
        
    #, 
    counts = (readership_data
        .groupby(["Year", "Quarter", "InvestorContact", "Provider", "ProviderContact"])
        .size()
        .rename("Count")
        .to_frame())
    
    repeat_counts = counts
    repeat_counts.rename({"Count" : "RepeatCount"}, axis=1, inplace=True)
    
    

    repeat_counts.RepeatCount -= 1 # We want to repeat counts so don't count the first one.
    repeat_counts = repeat_counts[repeat_counts.RepeatCount > 0].copy()
    
    #print(repeat_counts.query("Provider == 'CCB International Securities Ltd' and Year == 2019 and Quarter == 1"))

    df1 = repeat_counts.RepeatCount.groupby(["Year", "Quarter", "Provider"]).sum().rename("RepeatCount").to_frame()
    df1["PerQuarterPct"] = df1.RepeatCount.groupby(["Year", "Quarter"]).apply(lambda x: x / float(x.sum()))
    
    

    #df2 = repeat_counts
    df2 = repeat_counts.RepeatCount.groupby(["Year", "Quarter", "Provider", "ProviderContact"]).sum().rename("RepeatCount").to_frame()
    df2["PerProviderPct"] = df2.RepeatCount.groupby(["Year", "Quarter", "Provider"]).apply(lambda x: x / float(x.sum()))
    df2["PerQuarterPct"] = df2.RepeatCount.groupby(["Year", "Quarter"]).apply(lambda x: x / float(x.sum()))
    
    #interactions_data = interaction_data.set_index("Date")
    
    """
    interactions_data = (pd.DataFrame(interaction_data.ProviderContact.tolist(), index=interaction_data.ProviderContact.index)
        .stack()               
        .rename("ProviderContact")
        .reset_index())
    """

    #readership_data = readership_data[readershi
    conn = sqlite3.connect(':memory:')
    readership_data.query("ProviderContact != ''").to_sql('readership_data', conn, index=False)
    interaction_data.to_sql('interaction_data', conn, index=False)
    
    qry = '''
        select  
            min(readership_data.ReadershipRowId) as ReadershipRowId,
            interaction_data.InteractionRowId
        from
            readership_data 
            join interaction_data on
                lower(interaction_data.Provider) = lower(readership_data.Provider)
                and lower(interaction_data.ProviderContact) = lower(readership_data.ProviderContact)
                and lower(interaction_data.InvestorContact) = lower(readership_data.InvestorContact)            
                and interaction_data.Date >= readership_data.Date 
                and interaction_data.Year == readership_data.Year
                and interaction_data.Quarter == readership_data.Quarter
        group by 
            interaction_data.InteractionRowId
    '''

    mapping = pd.read_sql_query(qry, conn)
    
    readership_with_follow_on = (readership_data[readership_data["ProviderContact"] != ""]
       .merge(mapping.add_prefix("m_"), left_on='ReadershipRowId', right_on='m_ReadershipRowId')
       .merge(interaction_data.add_prefix("i_"), left_on='m_InteractionRowId', right_on='i_InteractionRowId'))
               
    follow_up_counts = (readership_with_follow_on
        .groupby(["Year", "Quarter", "Provider", "InvestorContact", "ProviderContact"])
        .size()
        .rename("Count")
        .to_frame())
    
    df3 = follow_up_counts.Count.groupby(["Year", "Quarter", "Provider"]).sum().rename("Count").to_frame()
    df3["PerQuarterCountPct"] = df3.Count.groupby(["Year", "Quarter"]).apply(lambda x: x / float(x.sum()))
    
    df4 = follow_up_counts
    df4["PerProviderPct"] = follow_up_counts.Count.groupby(["Year", "Quarter", "Provider"]).apply(lambda x: x / float(x.sum()))
    df4["PerQuarterPct"] = df4.Count.groupby(["Year", "Quarter"]).apply(lambda x: x / float(x.sum()))
    
    pairings = readership_with_follow_on[[
        "Year", "i_Year", "Quarter", "i_Quarter", "ReadershipRowId", "i_InteractionRowId", "Date", "i_Date", "InvestorContact", "i_InvestorContact", "Provider", "i_Provider", "ProviderContact", "i_ProviderContact", 
        "InteractionSubType","i_InteractionCategory", "Description", "i_Description"]]
    
    pairings.columns= [
        "ReadershipYear", "InteractionYear", "ReadershipQuarter", "InteractionQuarter", "ReadershipRowId", "InteractionRowId", "ReadershipDate", "InteractionDate", "ReadershipInvestorContact", "InteractionInvestorContact", 
        "ReadershipProvider", "InteractionProvider", "ReadershipProviderContact", "InteractionProviderContact", "RedershipSubType","InteractionCategory", "ReadershipDescription", "InteractionDescription"]
     
    df0 = df0.join(df1.RepeatCount)
    df0 = df0.fillna(0.0)
    df0["RepeatePct"] = df0.RepeatCount / df0.Count
        
    df0a = df0a.join(df2.RepeatCount)
    df0a = df0a.fillna(0.0)
    df0a["RepeatePct"] = df0a.RepeatCount / df0a.Count
    
    df1 = repeat_counts.RepeatCount.groupby(["Year", "Quarter", "Provider"]).sum().rename("RepeatCount").to_frame()
    df1["PerQuarterPct"] = df1.RepeatCount.groupby(["Year", "Quarter"]).apply(lambda x: x / float(x.sum()))

    
    writer = pd.ExcelWriter(f"d:/temp/impax_readership_2.xlsx", engine='xlsxwriter')
    write_df_to_excel_2(writer, df0, "AllReadsByProvider", 100)
    write_df_to_excel_2(writer, df0a, "AllReadsByAnalyst", 100)
    #write_df_to_excel_2(writer, df1, "RepeatReadsByProvider", 100)
    write_df_to_excel_2(writer, df2, "RepeatReadsByAnalyst", 100)
    write_df_to_excel_2(writer, df3, "ReadToInteractionByProvider", 100)
    write_df_to_excel_2(writer, df4, "ReadToInteractionByAnalyst", 100)
    write_df_to_excel_2(writer, pairings, "ReadershipInteractionPairings", 100)
    write_df_to_excel_2(writer, readership_data, "Readership", 100)
    write_df_to_excel_2(writer, interaction_data, "Interactions", 100)
    writer.save()
    
    df2 = None

    return df0, df0a, df1, df2, df3, df4

#%%

#%%
interactions, reads = read_data()

#%%

df0, df0a, df1, df2, df3, df4 = compute(interactions, reads)

#%%

raw_interactions =  pd.read_excel("D:/home/shane/Sync/Impax AA data/shane/Impax_Interactions_Combined column structure_post_SM.xlsx", sheet_name = 'Worksheet')

#%%

"""
Here is the process:
1.	Interactions already marked as Duplicate or Zerox3 are exclude from the universe of consideration.
2.	Duplicate groups are calculated on these tabs the column GroupId on the right indicates which interactions are in the same duplicate group.
All but one of the interactions in each group is excluded from the universe for consideration
3.	Repeats are calculated on the matching tags of the remaining interactions: "Year", "Quarter", "Provider", "InvestorContact", "ProviderContact"

This is what is excluded in step 2 that is not already excluded in Step 1.
-	Model interactions which have the same date but different subjects
-	Meeting interactions which have the same subject but different dates 

"""


#%%
def compute_repeat_interaction_counts(interactions):
    interactions = interactions[(interactions.ImpaxSpecificTag.isna()) & (interactions.ProviderContact.str.strip() != '')]

    model_interactions = interactions.query("InteractionCategory == 'Model Request'")
    model_columns = ["Date", "Provider", "InvestorContact", "InteractionCategory", "ProviderContact"] 
    unique_model_interactions = model_interactions.drop_duplicates(model_columns) 
    
    rm_interactions = interactions.query("InteractionCategory == 'Research Meeting'")
    rm_columns = ["Year","Quarter", "InvestorContact", "Provider", "InteractionCategory", "ProviderContact", "Description"] 
    unique_rm_interactions = rm_interactions.drop_duplicates(subset=rm_columns) 

    model_interactions.query("Year == 2019 and Quarter == 3 and ProviderContact == 'Marta Sousa Fialho'").to_excel("d:/tempMarta.xlsx")

    unique_interactions = pd.concat([
        interactions.query("InteractionCategory == 'Custom Request'"),
        unique_model_interactions,
        unique_rm_interactions])
   
    counts = (unique_interactions
        .groupby(["Year", "Quarter", "InvestorContact", "Provider", "ProviderContact"])
        .size()
        .rename("Count")
        .to_frame() -1).reset_index()

    counts = counts[counts.Count > 0]
  
    return counts

#%%

repeat_interactions_1 = compute_repeat_interaction_counts(interactions)

#%%

xls = pd.ExcelFile("./data/ImpaxData.xlsx")
repeat_interactions_0 = pd.read_excel(xls, "RepeatInteractionsByProvider")
all_reads_0 = pd.read_excel(xls, 'ReadershipCounts')

read_repeats_0 = pd.read_excel(xls, 'ReadershipRepeatCounts')
reads_to_interactions_0 = pd.read_excel(xls, 'ReadsToInteractions')

xls = pd.ExcelFile("d:/temp/impax_readership.xlsx")
all_reads_1 = df0 #pd.read_excel(xls, 'AllReadsByProvider', index_col=0)
read_repeats_1 = pd.read_excel(xls, 'RepeatReadsByProvider', index_col=0)
reads_to_interactions_1 = pd.read_excel(xls, 'ReadToInteractionByProvider', index_col=0)



all_reads_0a = all_reads_0.set_index(["Year", "Quarter", "Provider"])
all_reads_1a = all_reads_1.query("Year >= 2019").groupby(["Year", "Quarter", "Provider"]).Count.sum().to_frame()

read_repeats_0a = read_repeats_0.groupby(["Year", "Quarter", "Provider"]).RepeatCount.sum().to_frame()
read_repeats_1a = df1.query("Year >= 2019").groupby(["Year", "Quarter", "Provider"]).RepeatCount.sum().to_frame()



reads_to_interactions_0a = reads_to_interactions_0.set_index(["Year", "Quarter", "Provider"])
reads_to_interactions_1a = reads_to_interactions_1.set_index(["Year", "Quarter", "Provider"])

repeat_interactions_0a = repeat_interactions_0.set_index(["Year", "Quarter", "Provider"]).drop(columns="PerQuarterPct") 
repeat_interactions_1a = repeat_interactions_1.groupby(["Year", "Quarter", "Provider"]).Count.sum().rename("RepeatCount").to_frame()


"""
reads_to_interactions_0a = reads_to_interactions_0.set_index(["Year", "Quarter", "Provider"])
repeat_interactions_0a = repeat_interactions_0.set_index(["Year", "Quarter", "Provider"]).drop(columns="PerQuarterPct") 

repeat_interactions_1 = compute_repeat_interaction_counts(interactions)
repeat_interactions_1a = repeat_interactions_1.groupby(["Year", "Quarter", "Provider"]).Count.sum().rename("RepeatCount").to_frame()
"""
#reads_to_interactions_1a = reads_to_interactions_1.set_index(["Year", "Quarter", "Provider"])




assert_frame_equal(reads_to_interactions_0a, reads_to_interactions_1a)
assert_frame_equal(read_repeats_0a.sort_index(), read_repeats_1a.sort_index())
assert_frame_equal(repeat_interactions_0a.sort_index(), repeat_interactions_1a.sort_index())



#read_repeats_1a.reset_index().to_excel("d:/temp/repeats.xlsx")

#print(df1.query("Provider == 'CCB International Securities Ltd' and Year == 2019 and Quarter == 1"))
#print(read_repeats_0a.query("Provider == 'CCB International Securities Ltd' and Year == 2019 and Quarter == 1"))
#print(read_repeats_1a.query("Provider == 'CCB International Securities Ltd' and Year == 2019 and Quarter == 1"))


#%%

df =  pd.read_excel("./data/ImpaxData.xlsx", sheet_name = 'AllInteractions')
df.Date.dtype
# %%

pd._version.version_json