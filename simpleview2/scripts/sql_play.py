import time
import pandas as pd
import sqlalchemy


def execute_query(sql, cxn):
    start_time = time.time()
    res = pd.read_sql("set arithabort on; " + sql, cxn)
    elapsed_time = time.time() - start_time
    print(f"Executed in {elapsed_time} S")
    return res


engine = sqlalchemy.create_engine(
    'mssql+pyodbc://sa:dev@(local)/Operations?driver=ODBC+Driver+17+for+SQL+Server',
    connect_args={'autocommit': True}
)

cxn = engine.connect()
interactions_raw = execute_query("select top 10 * from aws_cur_data", cxn)